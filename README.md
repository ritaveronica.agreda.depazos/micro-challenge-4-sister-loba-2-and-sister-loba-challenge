# Master in Design for Emerging Futures
# Micro Challenge 4 MDEF / SISTER LOBA 2.0 and #SisterLobaChallenge
- Rita Veronica Agreda de Pazos & Francisco Flores
- MDEF 2020/21
- Institut d'Arquitectura Avançada de Catalunya, Elisava & Fab Lab Barcelona
## Table of Contents
1. [Fair Mobility and creative codesign with women cyclists](#fair-mobility-and-creative-codesign-with-women-cyclists)
2. [Context and Problems](#context-and-problems)
3. [Current situation](#current-situation)
4. [Sister Loba](#sister-loba)
5. [Concept](#concept)
6. [Methology and cocreation process](#methology-and-cocreation-process)
7. [Sister Loba Digital Device](#sister-loba-digital-device)
8. [The Sister Loba Challenge](#the-sister-loba-challenge)
9. [Final Presentation](#final-presentation)
10. [Concepts](#concepts)
11. [Skills](#skills)
## Fair Mobility and creative codesign with women cyclists
**Imagine a world where all people have equal rights and opportunities and where we can feel safe and connected to nature and our roots in every city...**
In this world **#_GenderEquality_** and **#_FairMobility_** are the norms!  

Since our fights are _Fair Mobility_ and _Bridging the Gender Gap_, we decided to create synergies and to invite women collectives as part of this challenge. We were inspired by women cyclists around the world.
## Context and problems
We were horrified by Saturday's April 24th breaking news: **Police are investigating reports of a Brit WhatsApp group allegedly promoting a supposed American TikTok video to call April 24th “_National Rape Day_” (The Sun)**

**The idea of encouraging sexual violence is just unacceptable for us.**

Since our fights involve _Fair Mobility_ and _Gender Gap_ issues, we started tackeling this problem in the FabAcademy’s Micro-challenge 3, we decided to go further in the Micro-challenge 4 trought co-creation and co-design. 
## Current situation 
- Climate Emergency
- Car industry and CO2 emissions
- Unfair Mobility and inclusion issues
- A need of 'humanizing data'
#### Women Safety is global issue. 
Globally, almost one in three women (an estimated 736 million) have experienced physical and/or sexual violence. 

Less than 40% of the women who experienced violence seek help of any sort. Very few look to formal institutions, such as police (less than 10%) and health services (UN Women, 2021).

![](Photos%20and%20videos/Gender_based_violence.png) 

Public transportation systems are not always well connected. Cycling is an affordable alternative to promote fair mobility, security and wellbeing. Listening to women sharing their cycling experience in the cities and involving them in the policy making process is imperative if we want to build fair and sustainable policies.
#### A need of 'Humanizing Data'
**Smart data** provides unprecedented opportunities for understanding and **planning urban mobility**. It makes huge amounts of information available with a high level of details, potentially precisely depicting overall macrotrends while also detecting micro-practices that elude traditional investigative approaches. Despite of this insight on mobility issues, **big data is not the ultimate solution for dealing with urban mobility**, especially when considering **social dimensions**. 

Mobility solutions builded on big data and the **qualitative information provided by citizens** could allow us to **reshape the cities and make them friendlier, gender inclusive and safer.**

Google's Map algorithms will find the shortest path and alternative routes in real time. 
- But, **are the shortest paths the safest ones for cyclists**? 
- **How can we build human centered big data based on the women’s cyclist experiences and needs**?
- How could this **‘humanized data' contribute to mobility planning** and policy approaches that may effectively promote cycling and enable women's security and wellbeing?  
- How can we build a **women’s cyclists sisterhood establishing sorority agreements**? 
## Sister Loba
We started this project in the third micro challenge by interviewing teenagers and young women who love cycling or who uses the bicycle as means of transportation to discover their perceptions, needs and desires. During this challenge we spread the universe of women cyclist and we talked to women from different communities, collectives and cities: from Fab Labers , MDEFers, schools' and univesities' students and teachers, the **_Biclot_** and **_Nestcity Lab_** communities, and delivery's services workers in Barcelona, to cyclist collectives like **_Mujeres en Bici_ **in Leon (Mexico) and **_Dame metro y medio de vida_** from Santa Cruz de la Sierra (8olivia).

![](Photos%20and%20videos/Mujeres_en_bici_Leon_18.jpg) ![](Photos%20and%20videos/Dame_metro_y_medio_de_vida.png) 

## Concept 
**Hermanadas/ Sisterhood movement of women cyclists**

Morgane Shaban inspired us with her protective charm: a **She-Wolf**. 

The idea of reproducing a herd of women cyclist was a perfect metaphor for a sorority mouvement!

**Our purpose is to empower women cyclists.** 

![](Photos%20and%20videos/Sisterloba_logo_eng.png)

After a co-creation process with women cyclists of our communities in Barcelona (Spain), Santa Cruz de la Sierra (Bolivia) and León (México) we designed a digital artifact and an analogue toolkit to allow them to map and rate their cyclist experience in their city in terms of safety and quality. Theirs insights are valuable while developing fair mobility practices and designing bottom-up policies.

We established the criteria that the future artifact had to fulfill:
1. Accessible
2. Affordable
3. Relevant and meaningful
4. Inclusive and eco friendly
5. A tool for generating relevant real-time data
6. Locally produced and replicable (globally distributed)
# Methology and cocreation process
Following the same methodology and steps we learned in the Microchallenges 2 and 3, when we co-desinged Pascal: The Chameleon,and She-Wolf 1.0, we decide to co-design a high tech solution (inspired in Morgan's protective charm) and a low tech toolkit for those cyclist who have no access to a smartphone or internet connection (like the women who deliver the journals every morning in Bolivia). 
## The Analogue Toolkit:
Contains a city map, a kit of 6 crayons, the instructions and description of the color code.

It **will allow women cyclists to rate and rank the bicycle paths and the alternative routes**. 

The toolkit is meant also to **be used by women who have no access to technologies or/and connectivity**.

![](Photos%20and%20videos/Analogue_final.jpg)
## The Digital Device: 
It's a DIY microcomputer attached to the bicycle that tracks the route coordinates and allows them to create maps based on the color code. 

The data is stored in a database. 

After rating the path, they can choose if they want to share the data or not. 

They can also simply download it and use it for the collective benefit. 

![](Photos%20and%20videos/Digital_on_a_bike.jpg)

## The Color Code: 
To create the Color Code for both solutions, the low tech and the high tech, and to be able to collect qualitative information and inputs we designed different forms for each variable: 
- Security (red)
- Infrastructure/speed/traffic/sign information (orange)
- Cycling skill’s level (yellow) 
- Connection with nature (green) 
- Inclusiveness/apt for children and elderly and (blue)
- Local identity/culture/cycle-tourism) (black)

![](Photos%20and%20videos/Cycle_tourism.png)![](Photos%20and%20videos/Ideal_for_families.png) 
![](Photos%20and%20videos/color_code.jpg)
# Sister Loba Digital Device
To DYI a Sister Loba Digital device to track and rate your cicling paths and routes.

![](Photos%20and%20videos/Digital_final_2.jpg)

You will need this materials and you can follow this steps:

### Materials and tools
- 3D Printer
- [PLA translucid filament](https://www.smartmaterials3d.com/en)
- 3D printed mold s
- Translucent tin cure Silicone with catalyser (mix ratio: 100A by 10B by weight)
- Silicone pigments (red and blue)
- CNC mini-milling machine
- PCB card

**Hardware components:**

_For Location Tracking:_
- ESP32 Adafruit HUZZAH32 Feather board
- 2 X ESP32 in headers
- Neo 6M GPS Module
- GPS pin headers
- 1 tactile push button switch
- 1 X 10K ohm Resistor
- 1 RGB LED ligh
- 1 X 100K ohm Resistor
- 1 X 150K ohm Resistor
- 1 Green LED ligh
- 1 X 220K ohm Resistor
- A 3.7V, 2000 mAh Lithium-Polymer rechargeable battery
- Few connection wires
- PCB card
- A smartphone, or a tablet, or a computer

_Software:_
- [Kicad](https://www.kicad.org/download/)
- [Inkscape](https://inkscape.org/release/inkscape-1.1/)
- Adobe Ilustrator
- [Rhino](https://www.rhino3d.com/download/)
- [Arduino](https://www.arduino.cc/en/software) IDE Software
- [IFTTT](https://ifttt.com/home)
- [TinyGPS++ Library](http://arduiniana.org/libraries/tinygpsplus/) 
- [Software Serial Library](https://www.arduino.cc/en/Reference/SoftwareSerial)
- [IFTTWebhook Library](https://www.arduino.cc/reference/en/libraries/iftttwebhook/)
- [Wire Library](https://www.arduino.cc/en/reference/wire)
- [WiFi Library](https://www.arduino.cc/en/Reference/WiFi)

_Features:_
- WiFi 
- Internet connection
### Making and integrating a PCB
- **Step 1:** Draw the circuits in [KiCad](https://www.kicad.org/download/)  and edit the vectors in [InkScape](https://inkscape.org/release/inkscape-1.1/) (_open source_) and  Adobe Illustrator. 
- **Step 2:** Export 3 different files (they must be perfectly alined) in 1000 dpi. The first file is for the engraving of the Traces, second one for the Holes where the ESP32 fits, and finally the Outline for cutting the board.
- **Step 3:** Set the CNC mini milling machine for the PCB fabrication. The milling process needs to be engraved and cutted in that order: traces, holes and outline.

![](Photos%20and%20videos/PCB_components.jpg) 

![](Photos%20and%20videos/SNAPPING_PCB_FAIR_MOBILITY.mp4) 
### Circuit Diagram:
**Connecting the GPS to the ESP32 Adafruit_HUZZAH32 Feather**

Pinouts diagram ESP32 Adafruit_HUZZAH32 Feather

![](Photos%20and%20videos/Adafruit_HUZZAH32_Feather_pinout.png) 

The NEO-6M GPS module has four pins: VCC, RX, TX, and GND. The module communicates with the ESP32 via serial communication using the TX and RX pins.

Pinouts diagram NEO-6M GPS module

![](Photos%20and%20videos/GPS_Neo_6M_labelled.jpg) 

#### Wiring

|  NEO-6M GPS Module	Wiring to | ESP32 Adafruit_HUZZAH32_Feather |
| ------ | ------ |
| **VCC**|  5V |
| **RX**| TX pin defined in the software serial (**pin 33**)|
| **TX**| RX pin defined in the software serial (**pin 32**)|
|**GND** |GND |

**Connecting the RGB LED to the ESP32 Adafruit_HUZZAH32 Feather**

To connect the RGB LED and to obtain the purple color, you can use this reference:

![](Photos%20and%20videos/RGB_LED_resistors.jpg)

|   RGB LED light Wiring to| ESP32 Adafruit_HUZZAH32_Feather |
| ------ | ------ |
| **Red** soldered to a 150K ohm Resistor | (pin **16**)  |
| **Green** | |
| **Blue**  soldered to a 100K ohm Resistor| (pin **17**)|
| **Cathode**  |**GND**|

**Connecting the Green LED to the ESP32 Adafruit_HUZZAH32 Feather**

To connect the Green LED follow:

|   RGB LED light Wiring to| ESP32 Adafruit_HUZZAH32_Feather |
| ------ | ------ |
| **Anode** soldered to a 220K ohm Resistor | (pin **13**)  |
| **Cathode** | **GND**|

Finally to connected the tactile push button to ESP32 Adafruit_HUZZAH32_Feather to send the location of the GPS follow:

|   Tactile push button Wiring to| ESP32 Adafruit_HUZZAH32_Feather |
| ------ | ------ |
| Tactile **push button** soldered to a 10K ohm Resistor | (pin **21**)  |

### Coding logic
**Before coding:**
1. Install the [TinyGPS++ Library](http://arduiniana.org/libraries/tinygpsplus/), to obtain data from the NEO-6M GPS module (it also have some usefull examples). 
2. Install the [Software Serial library](https://www.arduino.cc/en/Reference/SoftwareSerial) to define a conection to the pins.
3. Install the [IFTTWebhook Library](https://www.arduino.cc/reference/en/libraries/iftttwebhook/) to be able to receive the georeferenced locations in the database.
4. Install the [WiFi Library](https://www.arduino.cc/en/Reference/WiFi).
5. Install the [Wire Library](https://www.arduino.cc/en/reference/wire).

**How it works?**
1. To get raw GPS data by starting a serial communication with the GPS module we will use the **Software Serial Library**. With this library we initialized **serial communication**, for both ways: to see the readings on the serial monitor (output) and to communicate with the GPS module (input).
2. Connect the **pin 33** and **pin 32** as **RX** and **TX serial pins** to establish serial communication with the GPS module. 
3. Establish the **baud rate at 9600 bps**. It listens to the GPS serial port.
4. When data is received from the module, it is sent to the serial monitor. You can see the information in the Serial Monitor at **a baud rate of 9600**. You will obtain a bunch of information in the **GPS standard language, NMEA**. (NMEA stands for National Marine Electronics Association, and in the world of GPS, it is a standard data format supported by GPS manufacturers).
4. By using the **TinyGPS++ Library** you will convert those NMEA messages into a readable and useful format by saving the characters sequences into variables.
5. Only the latitude and longitude coordinates are taken from it using the TinyGPS library. The switch input based GPS needs a manual action to operates or send SMS or e-mails. It is to just send the location by ourselves. 
7. By pressing the button (input) the device will turn on and will connect to the **Wi-Fi** (a family of wireless network protocols, based on the IEEE 802.11 family of standards, which are commonly used for local area networking of devices and Internet access. WI-FI allows nearby digital devices to exchange data by radio waves. Since these are the most widely used computer networks in the world, used globally in home and small office networks to link desktop and laptop computers, tablet computers, smartphones, smart TVs, printers, and smart speakers together and to a wireless router to connect them to the Internet, and in wireless access points in public places like coffee shops, hotels, libraries and airports to provide the public Internet access for mobile devices we thought it was the most appropiate communication technology for the project). 
8. When the device is connected you will see a purple light (output).
9. When the device will find 3 satellites the green light will turn on (output).
10. By pressing the button again the GPS will start sending the data to a database using the **_If This Then That_ (commonly known as IFTTT)** service. (IFTT allows us to program a response to events in the world of various kinds. There is a long list of kinds of events to which IFTTT can respond, all detectable via the Internet. IFTTT has partnerships with hundreds of service providers that supply event notifications to IFTTT and execute commands that implement the responses, but some event and command interfaces are just public APIs. The programs, called applets, are simple and created graphically. We chose this technology because it allowed us to create programs and otherwise control IFTTT with a web interface, iOS or Android application).

![](Photos%20and%20videos/Digital_in_process.jpg) ![](Photos%20and%20videos/pcb.jpg)

![](Photos%20and%20videos/GPS_Readings.png)

### Code

```
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// Aactivating the devise

bool buttonPressed = false;
bool tracking = false;

static const int RXPin = 33, TXPin = 32;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

#include <WiFi.h>
#include <Wire.h>

// Connecting to Wifi

#define WIFI_SSID "add network"
#define  WIFI_PASSWORD "your password"
const char* resource = "/trigger/BIKE_GPS_READINGS/with/key/euLb8dHF6nGo2jawYmDQXW43umFZLVnYvR9C5bOYFGz";
const char* server = "maker.ifttt.com";

unsigned long previousMillis = 0;        // will store last time GPS was updated


const long interval = 5000;             // constants won't change:

void setup() {
  ss.begin(GPSBaud);

  pinMode(21, INPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);
  pinMode(13, OUTPUT);

  Serial.begin(115200);

  Serial.print("Connecting to ");
  WiFi.mode(WIFI_STA);   //   create wifi station
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  // GPS Characteristics 
  while (ss.available() > 0) {
    gps.encode(ss.read());
    if (gps.location.isUpdated()) {
      // Latitude in degrees (double)
      int sat = gps.satellites.value();
      Serial.println(sat);
      if (sat >= 3) {
        digitalWrite(13, HIGH);
        Serial.println("ON");
      }
      else {
        digitalWrite(13, LOW);
        Serial.println("OFF");
      }
      
//      // Horizontal Dim. of Precision (100ths-i32)
//      Serial.print("HDOP = ");
//      Serial.println(gps.hdop.value());
    }
  }

  if (digitalRead(21) == LOW && buttonPressed == false) {
    if (tracking == false) {
      digitalWrite(16, HIGH); // Code Neopixel Encender
      digitalWrite(17, HIGH);
      tracking = true;
    }
    else {
      digitalWrite(16, LOW); // Code Neopixel Apagar
      digitalWrite(17, LOW);
      tracking = false;
    }
    //tracking = true;
    buttonPressed = true;

  }

  if (digitalRead(21) == HIGH) {
    buttonPressed = false;
  }
  delay(10);

  if (tracking) {
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      // save the last time you blinked the LED
      previousMillis = currentMillis;

      makeIFTTTRequest();
      Serial.println("Data sent");
      //delay(1000);

    }
  }
}

void makeIFTTTRequest() {
  Serial.print("Connecting to ");
  Serial.print(server);

  WiFiClient client;
  int retries = 5;
  while (!!!client.connect(server, 80) && (retries-- > 0)) {
    Serial.print(".");
  }
  Serial.println();
  if (!!!client.connected()) {
    Serial.println("Failed to connect...");
  }

  Serial.print("Request resource: ");
  Serial.println(resource);

  Serial.println(gps.location.lat(), 6);

  // Value in datasheet
  String jsonObject = String("{\"value1\":\"") + String(gps.location.lat(), 6) + "\",\"value2\":\"" + String(gps.location.lng(), 6)
                      + "\",\"value3\":\"" + gps.speed.kmph() + "\"}";

  Serial.println(jsonObject);

  client.println(String("POST ") + resource + " HTTP/1.1");
  client.println(String("Host: ") + server);
  client.println("Connection: close\r\nContent-Type: application/json");
  client.print("Content-Length: ");
  client.println(jsonObject.length());
  client.println();
  client.println(jsonObject);

  int timeout = 5 * 10; // 5 seconds
  while (!!!client.available() && (timeout-- > 0)) {
    delay(100);
  }
  if (!!!client.available()) {
    Serial.println("No response...");
  }
  while (client.available()) {
    Serial.write(client.read());
  }

  Serial.println("\nclosing connection");
  client.stop();
}

 ```


 ## 3D Printing, Casting and Molding
To protect the electronics of the Digital Device from vibrations and different weather conditions, we designed a 3D printed case, a bike holder and a silicone wrap with the Sister's Loba logo.

The mold for casting the silicone was 3d printed in PLA.

Note: It's important to consider that not all silicones works with 3d printed PLA molds and you have to follow the security instructions like using vinyl gloves only (latex gloves inhibit the cure of the rubber) and glasses.

![](Photos%20and%20videos/milestones.jpg)
![](Photos%20and%20videos/PCB_integration.mp4) 

# The #SisterLobaChallenge
To test the Analogue Toolkit and the Digital Artifact we co-designed the **#SisterLobaChallenge**. 

![](Photos%20and%20videos/Start_mettingpoint.JPG) ![](Photos%20and%20videos/SisterLobaChallenge_flyer.JPG)

The leaders of the women cyclist collectives helped us to choose emblematic places, interesting and leisure places, tourist attractions, historic places, meeting points and iconic places as milestones.

![](Photos%20and%20videos/map_challenge.jpg)

We set up posters with a QR Code in the milestones to let them answer our questions. 

![](Photos%20and%20videos/SafetyQR.png) ![](Photos%20and%20videos/InfraestructureQR.png) ![](Photos%20and%20videos/NatureQR.png)

![](Photos%20and%20videos/Challenge_questions.jpg) ![](Photos%20and%20videos/Challenge_questions_2.jpg)

We invited them to choose their route in order to get out of their comfort zone, to join the challenge and live the fully experience while rating the paths and routes of their city.

![](Photos%20and%20videos/poster_challenge_insitu.jpg) ![](Photos%20and%20videos/Challenge_insituu.jpg)

During the challenge or after finishing it, women cyclists can draw and rate the paths based on our color code. They will decide if they want to share or not the georeferenced positions of the bike during the ride after rating the path that they chose. We respect the cyclist's privacy and their right to use their own information and also encourage sharing this data for collective benefit. It is necessary to visualize the problems that cyclists deal with every day in order to create fair policies.
While the cyclist were participating, pedestrians joined the challenge using the analogue toolkit.

# Value proposition of the Digital Device and the Toolkit
- Community building
- Sorority
- Friendly
- Technology
- Distributed Design + DYI
- Powerful Insights for Policy making

# Final presentation
Through GPS Tracker, Sister Loba women cyclist provided a valuable contribution to the co-creation process. We learned about sensors, materials, casting and molding and new ways of tackeling global issues like womens safety and fair mobility. Our solution enhances the creative skills of society as a whole and promotes an open-minded and ‘out-of-the-box’ approach to life.

![](Photos%20and%20videos/Digital_final_front.jpg)
![](Photos%20and%20videos/Digital_final_back.jpg)
![](Photos%20and%20videos/Digital_final_2.png)

## Future Development opportunities
How could we display the cycling experience on a map of the city?

How could we empower women cyclist and help them to impact in policy making?

![](Photos%20and%20videos/app_simulation.jpg)

# Concepts
- [x] What is a sensor
- [x] Physical, chemical, bio reactions
- [x] Closed loop systems
- [x] Complex post processing (computer vision)
- [x] Analog/digital read/write
- [ ] Types of actuators
- [ ] Electromagnetic outputs
- [ ] Power systems
- [ ] Power management
- [x] Design for manufacture (tolerances)
- [x] One sided mold
- [x] Two sided mold
- [x] Molding & casting applications
- [x] Materials for molding & casting
- [x] Machine molding (rough, finishing)

# Skills
- [x] Interpreting PinOut diagrams
- [x] Interpreting the serial monitor
- [x] Programming a board with sensors
- [x] Interpreting sensor data
- [x] Rate your electronics / sizing
- [x] Consumptions/amps/volts
- [x] Choose the components
- [x] Command of 3D modelling software
- [x] Command of milling machine
- [x] Molding and casting techniques
- [x] Programming a wifi/bluetooth enabled board
- [x] Interpret network protocols
- [x] Choose hardware for your communication
- [x] How to select a library

# Links to individual pages
Made with love by [Veronica](https://gitlab.com/ritaveronica.agreda.depazos/mdef-website) and [Paco](https://paco_flores.gitlab.io/mdef-2021/)  
![](Photos%20and%20videos/Departure_challenge_Vero_Paco.jpg)
![](Photos%20and%20videos/License_Fair_Mobility.png)
